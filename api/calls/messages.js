'use strict'

var options = require('../libs/options')

const messagesAPICall = {
  saveMessage: (apiInvoker, address, data) => {
    return apiInvoker(options('POST', address+"/api/saveMessage", data))
      .then(function (resp) {
        return resp
      })
      .catch(function (err) {
        console.log("getMessagesList failed");
      })
  },
  getMessagesList: (apiInvoker, address) => {
    return apiInvoker(options('GET', address+'/api/getMessagesList'))
      .then(function (resp) {
        return resp
      })
      .catch(function (err) {
        console.log("getMessagesList failed");
      })
  }
}

module.exports = messagesAPICall
