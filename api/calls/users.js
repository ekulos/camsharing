'use strict'

var options = require('../libs/options')

const usersAPICall = {
  getUsersList: (apiInvoker, address) => {
    return apiInvoker(options('GET', address+ '/api/getUsersList'))
      .then(function (resp) {
        return resp
      })
      .catch(function (err) {
        console.log("getUsersList failed");
      })
  }
}

module.exports = usersAPICall
