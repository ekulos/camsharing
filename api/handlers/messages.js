"use strict"

var MessagesSchema = require('../../schemas/messages')

const messagesAPI = {
  saveMessage: (req, res) => {
    const fields = req.body
    const item = new MessagesSchema(fields)

    item.save(function(err) {
      if (err) {
        res.send(err)
      }
      else{
        res.json(item)
      }
    })
  },
  getMessagesList: (req, res) => {
    MessagesSchema.find().sort({'time': 1}).limit(100).exec(function(err, list) {
      if (err){
        res.send(err)
      }
      else{
        res.json(list)
      }
    })
  }
}

module.exports = messagesAPI
