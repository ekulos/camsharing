"use strict"

var SnapshotsSchema = require('../../schemas/snapshots')

const snapshotsAPI = {
  saveSnapshot: (req, res) => {
    const fields = req.body
    let toSave = {
      username: fields.username,
      snapObj: {
        snap: fields.snap,
        time: fields.time
      }
    }
    const item = new SnapshotsSchema(toSave)

    SnapshotsSchema.findOneAndUpdate(
      {username: toSave.username},
      {$set: {snapObj: toSave.snapObj}},
      {upsert: true},
      function(err){
        if (err) {
          res.send(err)
        }
        else{
          res.json(item)
        }
      }
    )
  },
  getSnapshotsList: (req, res) => {
    SnapshotsSchema.find().exec(function(err, list) {
      if (err){
        res.send(err)
      }
      else{
        res.json(list)
      }
    })
  }
}

module.exports = snapshotsAPI
