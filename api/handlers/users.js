"use strict"

var UsersSchema = require('../../schemas/users')

const usersAPI = {
  saveUser: (req, res) => {
    const fields = req.body

    /*
    Usare string.js "npm install --save string"
    Parsing stringa e protezione caratteri speciali
    */

    const item = new UsersSchema(fields)

    UsersSchema.find({username: item.username}, function(err, docs){
      if(!err && !docs.length){
        item.save(function(err) {
          if (err) {
            res.send(err)
          }
          else{
            res.json(item)
          }
        })
      }
      else if (err){
        res.send(err)
      }
    })
  },
  getUsersList: (req, res) => {
    UsersSchema.find().select("username").exec(function(err, list) {
      if (err){
        res.send(err)
      }
      else{
        let formattedList = {}
        list.forEach((val, index) => {
          formattedList[val.username] = false
        })
        res.json(formattedList)
      }
    })
  },
  getUser: (req, res) => {
    /*
      Se nel body è specificato anche il parametro password allora questo viene
      preso in considerazione per il fetch dal db, altrimenti no.
      getUser è usata sia per controllare se uno username già esiste (Registrazione),
      sia per controllare le credenziali di autenticazione (Login).
    */
    const user = {
      username: req.body.username
    }
    if (req.body.password){
      user.password = req.body.password
    }
    UsersSchema.find(user).exec((err, docs) => {
      if(!err && docs.length){
        res.json({
          username: docs[0].username,
          userFound: true
        })
      }
      else if (!docs.length){
        res.json({
          userFound: false
        })
      }
    })
  }
}

module.exports = usersAPI
