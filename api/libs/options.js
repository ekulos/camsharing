const options = (method, address, body) => {
  var opts = {
    method,
    uri: address,
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
  }
  if(body){
    opts.body = body
  }
  return opts;
}

module.exports = options
