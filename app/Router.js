import React, { Component } from 'react'
import {
  Router,
  Route,
  IndexRoute,
  hashHistory
} from 'react-router'
import {
  Login,
  Registration
} from './containers/auth'
import { ChatPagePreloader } from './containers/chat'
import Layout from './components/Layout'

export default class MyRouter extends Component {
  render(){
    let indexRoute
    if(localStorage.getItem('username')){
      indexRoute = (<IndexRoute component={ChatPagePreloader}></IndexRoute>)
    }
    else{
      indexRoute = (<IndexRoute component={Login}></IndexRoute>)
    }
    return (
      <Router history={hashHistory}>
        <Route path="/" component={Layout}>
          {indexRoute}
          <Route path="login" component={Login} />
          <Route path="registration" component={Registration} />
          <Route path="chat" component={ChatPagePreloader} />
        </Route>
      </Router>
    )
  }
}
