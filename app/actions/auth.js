import { CALL_API } from 'redux-api-middleware'

export const tryLogin = (username, password) => {
  return {
    [CALL_API]: {
      endpoint: '/api/getUser',
      method: 'POST',
      body: JSON.stringify({
        username,
        password
      }),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'LOGIN_REQUEST',
        {
          type: 'LOGIN_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              (resp) => {
                return resp
              }
            )
          }
        },
        'LOGIN_ERROR'
      ]
    }
  }
}
