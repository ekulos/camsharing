export const showCallOveray = () => {
  return {
    type: "SHOW_MODAL",
    visibility: true
  }
}

export const hideCallOveray = () => {
  return {
    type: "HIDE_MODAL",
    visibility: false
  }
}

export const saveCallParams = (interlocutor, stream) => {
  return {
    type: "SAVE_CALL_PARAMS",
    interlocutor,
    streamObj: stream
  }
}
