export const saveOwnStream = (streamObj, src) => {
  return {
    type: 'SAVE_STREAM',
    streamObj,
    streamSrc : src
  }
}

export const saveSnapDictionary = (snapDictionary, username) => {
  return {
    type: 'SAVE_SNAP_DICTIONARY',
    snapDictionary,
    username
  }
}

export const updateSnapDictionary = (username, params, snapDictionary) => {
  return {
    type: 'UPDATE_SNAP_DICT',
    username,
    params,
    snapDictionary
  }
}

export const showChatOverlay = () => {
  return {
    type: "SHOW_OVERLAY",
    name: "chat"
  }
}

export const hideChatOverlay = () => {
  return {
    type: "HIDE_OVERLAY"
  }
}

export const initUsersList = (list) => {
  return {
    type: 'INIT_USERS_LIST',
    list
  }
}

export const updateUsersList = (list, username, logged) => {
  return {
    type: 'UPDATE_USERS_LIST',
    list,
    username,
    logged
  }
}

export const setSnapTimer = (snapTimer) => {
  return {
    type: 'SET_SNAP_TIMER',
    snapTimer
  }
}

export const newMessage = (notReadMessages) => {
  return {
    type: 'NEW_MESSAGE',
    notReadMessages
  }
}

export const readAllMessages = () => {
  return {
    type: 'MESSAGES_READ'
  }
}
