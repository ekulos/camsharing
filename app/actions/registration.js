import { CALL_API } from 'redux-api-middleware'

export const submitRegistration = (credentials) => {
  return {
    [CALL_API]: {
      endpoint: '/api/saveUser',
      method: 'POST',
      body: JSON.stringify(credentials),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'CREDENTIALS_REQUEST',
        {
          type: 'CREDENTIALS_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              (credentials) => {
                return {
                  username: credentials.username,
                  status: true
                }
              }
            )
          }
        },
        'CREDENTIALS_ERROR'
      ]
    }
  }
}

export const checkAccountAvailable = (username) => {
  return {
    [CALL_API]: {
      endpoint: '/api/getUser',
      method: 'POST',
      body: JSON.stringify({username}),
      headers: { 'Content-Type': 'application/json' },
      types: [
        'ACCOUNT_CHECK_REQUEST',
        {
          type: 'ACCOUNT_CHECK_RESULT',
          payload: (action, state, res) => {
            return res.json().then(
              (resp) => {
                let accountAvailable = false
                if(resp.userFound===false){
                  accountAvailable = true
                }
                return { accountAvailable }
              }
            )
          }
        },
        'ACCOUNT_CHECK_ERROR'
      ]
    }
  }
}
