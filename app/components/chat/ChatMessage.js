import React from 'react'
import moment from 'moment'

var ChatMessage = React.createClass({

  render: function () {
    var msg = this.props.message;
    var msgTime = moment(Number(msg.time))
    var hours = msgTime.hour()
    var minutes = msgTime.minute()
    hours = (hours < 9) ? '0' + hours : hours
    minutes = (minutes < 9) ? '0' + minutes : minutes
    return (
      <div className="chat-message">
        <div className="message-time">[{ hours + ':' + minutes }]</div>
        <div className="message-author">&lt;{msg.author}&gt;</div>
        <div className="message-content">{msg.content}</div>
      </div>
    );
  }
});

export default ChatMessage
