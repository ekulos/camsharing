import React from 'react'
import ReactDOM from 'react-dom'
import LinkedStateMixin from 'react-addons-linked-state-mixin'

var MessageInput = React.createClass({

  mixins: [LinkedStateMixin],

  handleClose: function(){
    this.props.handleClose()
  },

  keyHandler: function (event) {
    var msg = this.state.message.trim();
    if (event.keyCode === 13 && msg.length) {
      this.props.sendMessage(msg);
      this.setState({ message: '' });
    }
    if(event.keyCode === 27){
      this.handleClose()
    }
  },

  getInitialState: function () {
    return { message: '' };
  },

  componentDidMount(){
    window.x = ReactDOM.findDOMNode(this.inputText)
    ReactDOM.findDOMNode(this.inputText).focus()
  },

  render: function () {
    return (
      <input
        type="text"
        ref={(ref)=> this.inputText = ref}
        className = 'form-control'
        placeholder='Enter a message...'
        valueLink={this.linkState('message')}
        onKeyUp={this.keyHandler}
      />
    );
  }
});

export default MessageInput
