import React from 'react'
import ReactDOM from 'react-dom'
import { ChatMessage } from './'
import moment from 'moment'

var MessagesList = React.createClass({

  getInitialState: function () {
    return { messages: [] }
  },

  addMessage: function (message) {
    if(!message.time){
      //Caso in cui l'utente visualizza il suo stesso messaggio
      message.time = (+moment()).toString()
    }
    var messages = this.state.messages
    var container = ReactDOM.findDOMNode(this.refs.messageContainer)
    messages.push(message)
    this.setState({ messages: messages })
    // Smart scrolling - when the user is
    // scrolled a little we don't want to return him back
    if (container.scrollHeight -
        (container.scrollTop + container.offsetHeight) >= 50) {
      this.scrolled = true
    } else {
      this.scrolled = false
    }
  },

  componentDidUpdate: function () {
    if (this.scrolled) {
      return
    }
    var container = ReactDOM.findDOMNode(this.refs.messageContainer)
    container.scrollTop = container.scrollHeight
  },

  render: function () {
    var messages
    messages = this.state.messages.map(function (m) {
      return (
        <ChatMessage message={m} key={m.time}></ChatMessage>
      )
    })
    if (!messages.length) {
      messages = <div className="chat-no-messages">No messages</div>
    }
    return (
      <div className="messagesList" ref="messageContainer">
        {messages}
      </div>
    )
  }
})

export default MessagesList
