const React = require('react');
import ReactDOM from 'react-dom'
const initials = require('initials');
const contrast = require('contrast');
import FontIcon from 'material-ui/FontIcon';
import { UserSettings } from './'

// from https://flatuicolors.com/
//https://github.com/wbinnssmith/react-user-avatar
const defaultColors = [
  '#50a37b',
  '#3498db',
  '#5082a3',
  '#5052a3',
  '#b9bcbb'
];

function sumChars(str) {
  let sum = 0;
  for(let i = 0; i < str.length; i++) {
    sum += str.charCodeAt(i);
  }

  return sum;
}

class AvatarMenu extends React.Component {

  render() {
    let {
      borderRadius='100%',
      src,
      srcset,
      name,
      color,
      colors=defaultColors,
      size,
      style,
      onClick,
      className
    } = this.props;

    if (!name) throw new Error('AvatarMenu requires a name');

    const abbr = initials(name);
    size = size+"px";

    const imageStyle = {
      display: 'block',
      borderRadius
    };

    const innerStyle = {
      lineHeight: size,
      textAlign: 'center',
      borderRadius
    };

    if (size) {
      imageStyle.width = innerStyle.width = innerStyle.maxWidth = size;
      imageStyle.height = innerStyle.height = innerStyle.maxHeight = size;
    }

    let inner, classes = [className, 'AvatarMenu'];
    if (src || srcset) {
      inner = <img className="AvatarMenu--img" style={imageStyle} src={src} srcset={srcset} alt={name} />
    } else {
      let background;
      if (color) {
        background = color;
      } else {
        // pick a deterministic color from the list
        let i = sumChars(name) % colors.length;
        background = colors[i];
      }

      innerStyle.backgroundColor = background;

      inner = abbr;
    }

    if (innerStyle.backgroundColor) {
      classes.push(`AvatarMenu--${contrast(innerStyle.backgroundColor)}`);
    }

    const menuButton = (<div className="AvatarMenu--inner" style={innerStyle} >
                          <FontIcon className="material-icons">
                            {inner}
                          </FontIcon>
                        </div>)

    return (
      <div aria-label={name} className={classes.join(' ')} style={style}>
        <div ref='attachTo'></div>
        <UserSettings menuButton={menuButton} logout={this.props.logout}/>
      </div>
    )
  }
}

module.exports = AvatarMenu;
