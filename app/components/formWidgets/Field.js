import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap'


export default class Field extends Component {

  render() {
    const { children, help } = this.props
    const hasTooltip = help ? true : false
    const element = React.cloneElement(children, {hasTooltip, help})
    const input_name = children.type.name

    if (hasTooltip || input_name == 'Input') {
      return element
    } else {
      return (
        <Row>
          <Col md={6}>{element}</Col>
          <Col md={6}></Col>
        </Row>
      )
    }
  }

}
