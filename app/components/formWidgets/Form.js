import React, { Component, PropTypes } from 'react'
import Formsy from 'formsy-react'


Formsy.addValidationRule('isPositiveInt', (values, value) => {
  const posIntRE = /^(?:[+]?(?:0|[1-9]\d*))$/
  return posIntRE.test(value)
})

Formsy.addValidationRule('isPositiveDecimal', (values, value) => {
  const posDecRE = /^\d+(\.\d{1,2})?$/
  // /^(?:[+]?(?:\d+))?(?:\.\d*)$/
  return posDecRE.test(value)
})


class CustomForm extends Formsy.Form {

  validateForm = () => {
    let allIsValid;
    let inputs = this.inputs;
    let inputKeys = Object.keys(inputs);

    // We need a callback as we are validating all inputs again. This will
    // run when the last component has set its state
    const onValidationComplete = (function () {
      allIsValid = inputKeys.every((function (name) {
        const input = inputs[name]
        if (input)
          return input.state._isValid;
      }).bind(this));

      this.setState({
        isValid: allIsValid
      });

      if (allIsValid) {
        this.props.onValid();
      } else {
        this.props.onInvalid();
      }

      // Tell the form that it can start to trigger change events
      this.setState({
        canChange: true
      });
    }).bind(this);

    // Run validation again in case affected by other inputs. The
    // last component validated will run the onValidationComplete callback
    inputKeys.forEach((function (name, index) {
      const component = inputs[name];
      const validation = this.runValidation(component);
      if (validation.isValid && component.state._externalError) {
        validation.isValid = false;
      }
      component.setState({
        _isValid: validation.isValid,
        _isRequired: validation.isRequired,
        _validationError: validation.error,
        _externalError: !validation.isValid && component.state._externalError ? component.state._externalError : null
      }, index === inputKeys.length - 1 ? onValidationComplete : null);
    }).bind(this));

    // If there are no inputs, set state where form is ready to trigger
    // change event. New inputs might be added later
    if (!inputKeys.length && this.isMounted()) {
      this.setState({
        canChange: true
      });
    }
  }

  render () {
    const {
      noFormTag,
      ...nonFormsyProps
    } = this.props

    let element = (
      <form {...nonFormsyProps} formMethod='POST' onSubmit={this.submit}>
        {this.props.children}
      </form>
    )

    if (noFormTag) {
      element = (
        <div {...nonFormsyProps}>
          {this.props.children}
        </div>
      )
    }
    return element
  }
}


export default class Form extends Component {
  // XXX: Context is an experimental feature ...
  // XXX: https://facebook.github.io/react/docs/context.html

  static childContextTypes = {
    layout: PropTypes.string.isRequired,
    validatePristine: PropTypes.bool.isRequired,
    rowClassName: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
      PropTypes.object
    ]),
    labelClassName: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
      PropTypes.object
    ]),
    elementWrapperClassName: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array,
      PropTypes.object
    ])
  }

  constructor (props) {
    super(props)
    this.getLayoutClassName = this.getLayoutClassName.bind(this)
  }

  getChildContext () {
    return {
      layout: this.props.layout || 'vertical',
      validatePristine: this.props.validatePristine || false,
      rowClassName: this.props.rowClassName || '',
      labelClassName: this.props.labelClassName || '',
      elementWrapperClassName: this.props.elementWrapperClassName || ''
    }
  }

  getLayoutClassName () {
    return 'form-' + this.getChildContext().layout;
  }

  getForm() {
    // return the form instance
    return this.refs.formsy
  }

  render() {
    return (
      <CustomForm
        ref="formsy"
        className={this.getLayoutClassName()}
        {...this.props}
      >
        {this.props.children}
      </CustomForm>
    )
  }

}
