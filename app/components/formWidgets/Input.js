import React from 'react'
import {
  Input as BaseInput,
  Row as RowInput
} from 'formsy-react-components'
import { Col, Row } from 'react-bootstrap'
import { getTooltip, showSuccess, getExtraRowProps, getNCols, getRowClass } from './base'


export default class Input extends BaseInput  {
  showSuccess = () => {
    return showSuccess(this)
  }

  getExtraRowProps() {
    return getExtraRowProps(this)
  }

  getNCols = () => {
    return getNCols(this)
  }

  getTooltip = (n_cols) => {
    return getTooltip(this, n_cols)
  }

  getRowClass = () => {
    return getRowClass(this)
  }

  render () {
    let element = this.renderElement()
    const layout = this.getLayout()
    const n_cols = this.getNCols()


    if (this.props.type === 'hidden') {
      return element
    }

    if (layout === 'elementOnly') {
      return element
    }

    if (this.props.addonBefore || this.props.addonAfter || this.props.buttonBefore || this.props.buttonAfter) {
      element = this.renderInputGroup(element);
    }
    return (
      <Row className={this.getRowClass()}>
        <Col md={n_cols.left}>
          <RowInput
              {...this.getRowProperties()}
              htmlFor={this.getId()} >
              {element}
          </RowInput>
        </Col>
        {this.getTooltip(n_cols)}
      </Row>
    )
  }
}
