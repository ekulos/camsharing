import React from 'react'
import { HOC } from 'formsy-react'
import {
  Input as BaseInput,
  Row as RowInput
} from 'formsy-react-components'
import InputPassword from 'react-ux-password-field'

import { Col, Row } from 'react-bootstrap'
import { getTooltip, showSuccess, getExtraRowProps, getNCols, getRowClass } from './base'

class PasswordInput extends BaseInput  {

  constructor(props) {
    super(props)
    this.customChangeValue = this.customChangeValue.bind(this)
  }

  customChangeValue(value) {
    this.setValue(value)
  }

  showSuccess = () => {
    return showSuccess(this)
  }

  getExtraRowProps() {
    return getExtraRowProps(this)
  }

  getNCols = () => {
    return getNCols(this)
  }

  getTooltip = (n_cols) => {
    return getTooltip(this, n_cols)
  }

  getRowClass = () => {
    return getRowClass(this)
  }

  renderElementCustom () {
    const STRENGTH_LANG = ['Bad', 'Not good', 'Decent', 'Strong', 'Great']

    return (
      <InputPassword
        placeholder="Password*"
        className="form-control"
        minScore={2}
        name="password"
        strengthLang={STRENGTH_LANG}
        ref="element"
        id={this.getId()}
        onChange={this.customChangeValue}
        value={this.getValue()}
      />
    )
  }

  render () {
    const element = this.renderElementCustom()
    const n_cols = this.getNCols()
    if (this.getLayout() === 'elementOnly') {
      return element;
    }

    return (
      <Row className={this.getRowClass()}>
        <Col md={n_cols.left}>
          <RowInput
              {...this.getRowProperties()}
              htmlFor={this.getId()} >
              {element}
          </RowInput>
        </Col>
        {this.getTooltip(n_cols)}
      </Row>
    )
  }
}

export default HOC(PasswordInput)
