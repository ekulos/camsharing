import React, { Component } from 'react'
import {
  Input,
  PasswordInput
}
from './'

export default class PasswordWidget extends Component {

  renderPasswordInput() {
    const PWD_REGEX = /^(?=.*[A-Za-z])[A-Za-z]{6,}$/
    return (
      <PasswordInput
        name="password"
        label="Password"
        validations={{
          minLength: 6,
          matchRegexp: PWD_REGEX
        }}
        validationErrors={{
          minLength: 'Please, type at least 6 characters.',
          matchRegexp: 'Please, type at least a lowercase character, ' +
                       'an uppercase character.'
        }}
        fullwidth
        required
      />
    );
  }

  renderRetypePasswordInput () {
    return (
      <Input
        type='password'
        label='Confirm Password'
        name='passwordConfirm'
        placeholder='Repeat password*'
        validations='equalsField:password'
        validationErrors={{
          equalsField: 'Passwords do not match'
        }}
        fullwidth
        required
      />
    )
  }

  render () {
    let password_wdgt = this.renderPasswordInput()
    let retype_password_wdgt = this.renderRetypePasswordInput()

    return (
      <div>
        {password_wdgt}
        {retype_password_wdgt}
      </div>
    )
  }

}
