import React from 'react'
import ReactDOM from 'react-dom'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import Popover from 'material-ui/Popover'
import ActionExitToApp from 'material-ui/svg-icons/action/exit-to-app'
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
import { SnapTimerSubmenu } from '../../containers/chat'

injectTapEventPlugin()

class UserSettings extends React.Component {
  constructor(props, context) {
    super(props, context)

    this.handleRequestClose = this.handleRequestClose.bind(this)
    this.handleRequestOpen = this.handleRequestOpen.bind(this)

    this.state = {
      open: false
    }
  }

  handleRequestClose() {
    if(this.state){
      this.setState({
        open: false
      })
    }
  }

  handleRequestOpen(){
    if(this.state){
      this.setState({
        open: true
      })
    }
  }

  //vedere icone https://design.google.com/icons
  render() {
    const menuButton = (<div onTouchTap={this.handleRequestOpen}>
                          {this.props.menuButton}
                        </div>)

    const menuList = () => {
      console.log("attachTo", this.refs.attachTo)
      if(this.state.open){
        return(
          <Popover
            open={this.state.open}
            anchorEl={this.refs.attachTo}
            onRequestClose={this.handleRequestClose}
            canAutoPosition={false}
          >
            <Menu>
              <MenuItem
                primaryText="Logout"
                leftIcon={<ActionExitToApp />}
                onClick={this.props.logout}
              />
              <Divider />
              <SnapTimerSubmenu
                handleRequestClose={this.handleRequestClose}
              />
            </Menu>
          </Popover>
        )
      }
    }

    return(
      <MuiThemeProvider>
        <div>
          {menuButton}
          <div>
            {menuList()}
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}

export default UserSettings
