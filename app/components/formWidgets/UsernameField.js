import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Input,
  Field
} from './'
import {
  checkAccountAvailable
} from '../../actions'

class UsernameField extends Component {

  wrapField = (element, help='') => (
    <Field help={help}>{element}</Field>
  )

  render(){
    let inputField = (
      <Input
        type="text"
        placeholder="Username*"
        name="username"
        onBlur={
          (event) => {
            this.props.checkAccountAvailable(event.target.value)
          }
        }
        hideTooltip={this.props.accountAvailable!==false}
        fullwidth
        required
      />
    )
    return (
      <div>
        {this.wrapField(inputField, "Account unavailable!")}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      accountAvailable: state.registration.accountAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkAccountAvailable: (username) => {
      dispatch(checkAccountAvailable(username))
    }
  }
}

UsernameField = connect(mapStateToProps, mapDispatchToProps)(UsernameField)

export default UsernameField
