import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import { tryLogin } from '../../actions'
import { remountComponents } from '../../lib'
import {
  Form,
  Input
} from '../../components/formWidgets'
import NotificationSystem from 'react-notification-system'
import { Link } from 'react-router'

class Login extends Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props){
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(formValues){
    this.props.tryLogin(formValues.username, formValues.password)
  }
  
  componentWillMount(){
    this.props.resetStore()
  }

  componentDidMount(){
    if(this.props.accountAvailable===true){
      let username = this.props.username
      this.props.notifAppearsLogin.addNotification({
        message: 'Congratulations '+ username +' you\'re now member of our community!',
        title: "Registered!",
        level: 'success'
      })
    }
  }

  componentWillUpdate(nextProps){
    if(localStorage.getItem('username')){
      remountComponents(this, 'chat')
    }
    if(nextProps.userFound===false){
      this.notifAppearsLogin.addNotification({
        message: 'Wrong credentials',
        title: "Error!",
        level: 'error',
        uid: "wrong_credentials"
      })
    }
  }

  render(){
    return (
      <div className="auth-page">
        <NotificationSystem ref={(ref) => this.notifAppearsLogin = ref} />
        <div className="pen-title">
          <h1>CamSharing Portal</h1>
        </div>
        <div className="form-module">
          <div className="form">
            <h2>Login to your account</h2>
            <Form
              onSubmit={this.onSubmit}
            >
              <Input
                type="text"
                name="username"
                placeholder="Username*"
                value="pan"
                hideSuccessStatus
                required
                fullwidth
              />
              <Input
                type="password"
                name="password"
                placeholder="Password*"
                value="lapanenkalapanenka"
                hideSuccessStatus
                required
                fullwidth
              />
              <button
                type="submit"
                className="btn btn-primary"
              >
                Login
              </button>
            </Form>
          </div>
          <div className="cta">
            <Link
              to="registration"
            >
              get an account
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.auth){
    ret = {
      ...ret,
      userFound: state.auth.userFound
    }
  }
  if(state.registration){
    ret = {
      ...ret,
      username: state.registration.username,
      accountAvailable: state.registration.accountAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    tryLogin: (username, password) => {
      dispatch(tryLogin(username, password))
    },
    resetStore: () => {
      dispatch({type:'RESET_STORE'})
    }
  }
}

Login = connect(mapStateToProps, mapDispatchToProps)(Login)

export default Login
