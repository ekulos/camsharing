import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  submitRegistration
} from '../../actions'
import {
  Input,
  Form,
  PasswordWidget,
  UsernameField,
  Cropper
} from '../../components/formWidgets'
import { remountComponents } from '../../lib'
import { Link } from 'react-router'

import '../../styles/auth/main.css'

class Registration extends Component {

  mapInputs(inputs) {
    return {
      username: inputs.username,
      password: inputs.password,
      email: inputs.email,
      phone: inputs.phone
    }
  }

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(props){
    super(props)

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(formValues){
    this.props.submitRegistration(formValues)
  }

  componentWillUpdate(nextProps){
    if(nextProps.registrationStatus){
      remountComponents(this, 'login')
    }
  }

  render(){
    return (
      <div className="auth-page">
        <div className="pen-title">
          <h1>CamSharing Portal</h1>
        </div>
        <div className="form-module">
          <div className="form">
            <h2>Create an account</h2>
            <Form
              mapping={this.mapInputs}
              onSubmit={this.onSubmit}
            >
              <Input
                type="text"
                placeholder="Name*"
                name="name"
                validations="isAlphanumeric"
                value=""
                hideTooltip
                required
              />
              <Input
                type="text"
                placeholder="Last Name*"
                name="lastName"
                validations="isAlphanumeric"
                value=""
                hideTooltip
                required
              />
              <UsernameField />
              {/*
                <Cropper />
              */}
              <PasswordWidget />
              <Input
                type="email"
                placeholder="Email Address*"
                name="email"
                validations="isEmail"
                value="test@gmail.com"
                hideTooltip
                required
              />
              <button
                type="submit"
                className="btn btn-primary"
                disabled={!this.props.accountAvailable}
              >
                Register
              </button>
            </Form>
          </div>
          <div className="cta">
            <Link
              to="login"
            >
              &lt;&nbsp;Back
            </Link>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.registration){
    ret = {
      registrationStatus: state.registration.status,
      accountAvailable: state.registration.accountAvailable
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    submitRegistration: (credentials) => {
      dispatch(submitRegistration(credentials))
    }
  }
}

Registration = connect(mapStateToProps, mapDispatchToProps)(Registration)

export default Registration
