import React from 'react'
import { connect } from 'react-redux'
import {
  Button,
  Modal
} from 'react-bootstrap'
import Spinner from 'react-loader'
import { hideCallOveray } from '../../actions'

const modalStyle = {
  position: 'fixed',
  zIndex: 1040,
  top: 0, bottom: 0, left: 0, right: 0
};

const backdropStyle = {
  ...modalStyle,
  zIndex: 'auto',
  backgroundColor: '#000',
  opacity: 0.5
};

const dialogStyle = function() {
  const availWidth = screen.availWidth * (3/4)
  const left = (screen.availWidth * (1/4)) / 2

  return {
    position: 'absolute',
    width: availWidth,
    top: 0,
    left: -left,
    border: '1px solid #e5e5e5',
    backgroundColor: 'white',
    boxShadow: '0 5px 15px rgba(0,0,0,.5)',
    padding: 20
  };
};


let CallOverlay = React.createClass({
  setHeader(){
    this.refs.headerMessage.innerText = "In call with " + this.props.interlocutor
  },

  render() {
    let videoTag = (<Spinner scale={0.50}></Spinner>)
    let headerMessage

    if(this.props.interlocutor_streamObj){
      window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL
      const src = window.URL.createObjectURL(this.props.interlocutor_streamObj)
      videoTag = (<video autoPlay src={src} onCanPlayThrough={this.setHeader.bind(this)} />)
      headerMessage = "CALLING " + this.props.interlocutor + "..."
    }

    return (
      <Modal
        aria-labelledby='modal-label'
        style={modalStyle}
        backdropStyle={backdropStyle}
        show={this.props.activeCall}
        onHide={this.props.hideCallOveray}
      >
        <div style={dialogStyle()} >
          <h4 ref="headerMessage">
            { headerMessage }
          </h4>
          <div>
            { videoTag }
          </div>
        </div>
      </Modal>
    );
  }
});

const mapStateToProps = (state, ownProps) => {
  let ret = {
    ...ownProps
  }
  if(state.call){
    ret = {
      ...ret,
      interlocutor: state.call.interlocutor,
      interlocutor_streamObj: state.call.interlocutor_streamObj,
      activeCall: state.call.activeCall
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    hideCallOveray: () => {
      dispatch(hideCallOveray())
    }
  }
}

CallOverlay = connect(mapStateToProps, mapDispatchToProps)(CallOverlay)

export default CallOverlay
