import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  CallOverlay
} from './'
import {
  saveSnapDictionary,
  updateSnapDictionary,
  showCallOveray,
  hideCallOveray,
  saveCallParams
} from '../../actions'
import {
  Row as RowFlex,
  Col as ColFlex
} from 'react-flexbox-grid'
import {
  Image
} from 'react-bootstrap'

class CamStream extends React.Component {

  constructor(props){
    super(props)
    const chatProxy = this.props.chatProxy

    //binding chatProxy listeners & chatPage callbacks
    chatProxy.saveSnapDictionary(this.saveSnapDictionary.bind(this))
    chatProxy.startCall(this.startCall.bind(this))
    chatProxy.closeCall(this.closeCall.bind(this))
    chatProxy.updateSnapDictionary(this.updateSnapDictionary.bind(this))

    this.userSnapshotsSection = this.userSnapshotsSection.bind(this)
    this.requestCall = this.requestCall.bind(this)
    this.calcImageElem = this.calcImageElem.bind(this)
  }

  calcImageElem(sd, username){
    let calculatedClassName = sd[username].availability + " availability-box"
    return(
      <ColFlex md={3} className="box" key={username}>
        <div>
          <Image
            height={this.props.height}
            width={this.props.width}
            src={sd[username].snap}
          />
          <div className="box-username">
            {username}
            <span className={calculatedClassName}>o</span>
          </div>
        </div>
      </ColFlex>
    )
    //onClick={() => this.requestCall(username)}
  }

  saveSnapDictionary(snapDictionary){
    this.props.saveSnapDictionary(snapDictionary, this.props.username)
  }

  updateSnapDictionary(username, params){
    this.props.updateSnapDictionary(username, params, this.props.snapDictionary)
  }

  requestCall(interlocutor){
    this.chatProxy.requestCall(interlocutor, this.props.streamObj)
    this.props.showCallOveray()
  }

  startCall(interlocutor, stream){
    this.props.saveCallParams(interlocutor, stream)
  }

  closeCall(){
    this.props.hideCallOveray()
  }

  userSnapshotsSection(){
    let snaps = []
    const snapDictionary = this.props.snapDictionary

    if(snapDictionary){
      Object.keys(snapDictionary).forEach((username) => {
        let elem = (this.calcImageElem(snapDictionary, username))
        snaps.push(elem)
      })
    }

    return snaps
  }

  componentWillUpdate(nextProps){
    if(this.props.snapTimer !== nextProps.snapTimer){
      this.props.startTimer(nextProps.snapTimer)
    }
  }

  render(){
    console.log("camstream render", this.props.chatProxy.id)
    return (
      <RowFlex>
        { this.userSnapshotsSection() }
        <CallOverlay />
      </RowFlex>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ret,
      snapDictionary: state.chat.snapDictionary,
      snapTimer: state.chat.snapTimer
    }
  }
  if(state.call){
    ret = {
      ...ret,
      activeCall: state.call.activeCall
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveSnapDictionary: (snapDictionary, username) => {
      dispatch(saveSnapDictionary(snapDictionary, username))
    },
    updateSnapDictionary: (username, params, snapDictionary) => {
      dispatch(updateSnapDictionary(username, params, snapDictionary))
    },
    showCallOveray: () => {
      dispatch(showCallOveray())
    },
    hideCallOveray: () => {
      dispatch(hideCallOveray())
    },
    saveCallParams: (interlocutor, streamObj) => {
      dispatch(saveCallParams(interlocutor, streamObj))
    }
  }
}

CamStream = connect(mapStateToProps, mapDispatchToProps)(CamStream)

export default CamStream
