import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  MessageInput,
  MessagesList
} from '../../components/chat'
import {
  hideChatOverlay,
  newMessage
} from '../../actions'
import {
  Panel
} from 'react-bootstrap'
import {
  Transition
} from 'react-overlays'

class ChatOverlay extends Component {

  componentDidMount(){
    this.onMessage = this.onMessage.bind(this)
    this.addMessage = this.addMessage.bind(this)
    this.sendMessage = this.sendMessage.bind(this)
    this.initMessagesList = this.initMessagesList.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.onMessage(this.onMessage)
    chatProxy.onMessagesList(this.initMessagesList)
    chatProxy.requestMessagesList()
  }

  initMessagesList(messagesList){
    messagesList.forEach((item) => {
      this.addMessage(item)
    })
  }

  sendMessage(text) {
    const message = {
      content: text,
      author: this.props.username
    }

    //Invia il messaggio agli altri
    this.props.chatProxy.broadcastMessage(message)

    //Visualizza il messaggio nella chat locale
    this.refs.messagesList.addMessage(message)
  }

  onMessage(message){
    this.props.newMessage()
    this.addMessage(message)
  }

  addMessage(message){
    this.refs.messagesList.addMessage(message)
  }

  render(){
    return (
      <Transition
        in={this.props.visibleOverlay === 'chat'}
        timeout={1000}
        className='notShownOverlay'
        enteredClassName='shownOverlay'
        enteringClassName='shownOverlay'
      >
        <div className="container">
          <div className="buttonClose">
            <button onClick={this.props.hideChatOverlay}>Close Modal</button>
          </div>
          <Panel>
            <MessagesList ref="messagesList" />
            <MessageInput
              ref="messageInput"
              sendMessage={this.sendMessage}
              handleClose={this.props.hideChatOverlay}
            />
          </Panel>
        </div>
      </Transition>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {
    ...ownProps
  }
  if(state.chat){
    ret = {
      ...ret,
      visibleOverlay: state.chat.visibleOverlay,
      notReadMessages: state.chat.notReadMessages
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    hideChatOverlay: () => {
      dispatch(hideChatOverlay())
    },
    newMessage: () => {
      dispatch(newMessage())
    }
  }
}

ChatOverlay = connect(mapStateToProps, mapDispatchToProps)(ChatOverlay)

export default ChatOverlay
