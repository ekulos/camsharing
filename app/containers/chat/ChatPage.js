import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import {
  CamStream,
  UsersList,
  ChatOverlay
} from './'
import {
  saveOwnStream,
  showChatOverlay
} from '../../actions'
import {
  AvatarMenu
} from '../../components/formWidgets'
import { remountComponents } from '../../lib'
import {
  PageHeader,
  Grid as GridBoot,
  Row as RowBoot,
  Col as ColBoot
} from 'react-bootstrap'
import {
  Grid as GridFlex
} from 'react-flexbox-grid'
import cron from 'node-cron'
import NotificationSystem from 'react-notification-system'
import getChatProxyInstance from '../../models/ChatProxy'

import '../../styles/chat/main.css'

class ChatPage extends Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  constructor(){
    super()
    this.chatProxy = getChatProxyInstance()

    this.leavePageHandler = this.leavePageHandler.bind(this)
    this.startTimer = this.startTimer.bind(this)
    this.showNotification = this.showNotification.bind(this)
    this.userConnected = this.userConnected.bind(this)
    this.userDisconnected = this.userDisconnected.bind(this)
    this.handleLogout = this.handleLogout.bind(this)

    //binding chatProxy listeners & chatPage callbacks
    this.chatProxy.onUserConnected(this.userConnected)
    this.chatProxy.onUserDisconnected(this.userDisconnected)
  }

  componentWillMount(){
    this.chatProxy.connectToServer(this.props.username)
    this.firstConnection = true
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia
    navigator.getUserMedia({ audio: true, video: { width: 1280, height: 720 } },
                             (stream) => {
                               console.log("success");
                               window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL
                               const src = window.URL.createObjectURL(stream)
                               this.props.saveOwnStream(stream, src)
                               this.chatProxy.registerCallListeners(stream, this.camRef)
                               this.hasWebcam = true
                               this.startTimer(this.props.snapTimer)
                               this.firstConnection = false
                             },
                             (e) => {
                               console.log("no user media")
                               this.hasWebcam = false
                               this.startTimer(this.props.snapTimer)
                               this.firstConnection = false
                             }
                          )
  }

  componentDidMount(){
    //function calls
    //this.leavePageHandler()
    this.showNotification(this.props.username, "WELCOME")
  }

  leavePageHandler(){
    window.onbeforeunload = function() {
      return 'Are you sure you want to leave this page?'
    }
  }

  startTimer(snapTimer){
    const camRef = this.camRef
    const onTimerTick = (firstConnection) => {
      camRef.src = this.props.streamSrc
      //camRef.mozSrcObject = this.props.streamObj
      //console.log(this.props.streamObj);

      if(camRef && this.hasWebcam){
        camRef.onloadedmetadata = (e) => {
          camRef.play().then(() => {
            let done = this.chatProxy.takeSnapshot(camRef, firstConnection)
            if(done){
              camRef.pause()
              camRef.src=''
            }
          }, (e) => {
            console.log('play error',e);
          })
        }
      }

      /*
      L'evento play esegue takeSnapshot che a sua volta invia al server la sua
      singola snapshot (FASE SEND SNAP)

      requestSnap richiede al server la snapshot di tutti (FASE REQUEST SNAP)
      */

      this.chatProxy.requestSnap()
    }

    onTimerTick(this.firstConnection)
    if(this.timer){
      this.timer.stop()
    }
    this.timer = cron.schedule('*/' + snapTimer + ' * * * * *', () => {
      onTimerTick(false)
    })
  }

  showNotification(username, type) {
    let message = 'User '+username
    let level, title
    switch (type) {
      case "CONNECTED":
        message += " logged in"
        title = 'Connection',
        level = "success"
        break
      case "DISCONNECTED":
        message += " logged out",
        title = 'Disconnection',
        level = "error"
        break
      case "WELCOME":
        message = "Hi "+username+"!"
        title = "Welcome"
        level = "info"
        break
      default:
        message = ""
        level = "info"
    }
    this.notifAppearsChat.addNotification({
      position: 'br',
      message,
      title,
      level
    })
  }

  userConnected(username){
    console.log("utente connesso");
    this.showNotification(username, "CONNECTED")
  }

  userDisconnected(username){
    this.showNotification(username, "DISCONNECTED")
  }

  handleLogout(){
    this.chatProxy.disconnect()
    localStorage.removeItem('username')
    if(this.timer){
      this.timer.stop()
    }
    remountComponents(this, 'login')
  }

  render(){
    console.log("my name is", this.props.username)
    console.log("render chat page");
    return (
      <div>
        <NotificationSystem ref={(ref) => this.notifAppearsChat = ref} />
        <header>
          <div className="container">
            <ColBoot md={8}>
              <PageHeader>CamSharing</PageHeader>
            </ColBoot>
            <ColBoot md={4} id="buttons">
              <ColBoot md={3}>
                <AvatarMenu
                  size="48"
                  name={this.props.username}
                  logout={this.handleLogout}
                />
              </ColBoot>
              <ColBoot md={3}>
                <select onChange={(e) => {this.chatProxy.onChangeAvailability(e.target.value)}}>
                  <option>available</option>
                  <option>unavailable</option>
                </select>
              </ColBoot>
              <ColBoot md={3}>
                <button type="button" className="btn todo" >todo</button>
              </ColBoot>
              <ColBoot md={3}>
                <button type="button" className="btn btn-primary" onClick={this.props.showChatOverlay}>Chat</button>
              </ColBoot>
            </ColBoot>
          </div>
        </header>
        <div>
          <video
            muted
            style={{display:"none"}}
            ref={(ref) => this.camRef = ref }
            height={150}
            width={200}
          />
          <section className="mainSection">
            <GridBoot>
              <ColBoot md={9} className="snapshotsColumn">
                <GridFlex>
                  <CamStream
                    height={182}
                    width={182}
                    chatProxy={this.chatProxy}
                    username={this.props.username}
                    startTimer={this.startTimer}
                  />
                </GridFlex>
              </ColBoot>
              <ColBoot md={3} className="users">
                <UsersList chatProxy={this.chatProxy} />
              </ColBoot>
            </GridBoot>
          </section>
          <ChatOverlay
            chatProxy={this.chatProxy}
            username={this.props.username}
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  let ret = {}
  if(state.chat){
    ret = {
      ...ret,
      streamObj : state.chat.streamObj,
      streamSrc : state.chat.streamSrc
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveOwnStream: (mediaStreamObj, src) => {
      dispatch(saveOwnStream(mediaStreamObj, src))
    },
    showChatOverlay: () => {
      dispatch(showChatOverlay())
    }
  }
}

ChatPage = connect(mapStateToProps, mapDispatchToProps)(ChatPage)

export default ChatPage
