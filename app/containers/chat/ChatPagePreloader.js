import React from 'react'
import { connect } from 'react-redux'
import { remountComponents } from '../../lib'
import { ChatPage } from './'
import {
  setSnapTimer
} from '../../actions/chat'

class ChatPagePreloader extends React.Component {

  static contextTypes = {
    router: React.PropTypes.object
  }

  componentWillMount(){
    if(!localStorage.getItem('username')){
      remountComponents(this, 'login')
    }
    else{
      this.snapTimer = 5
      this.props.setSnapTimer(this.snapTimer)
    }
  }

  render() {
    let toRender
    let username = localStorage.getItem('username')
    if(username){
      toRender = (<ChatPage username={username} snapTimer={this.snapTimer}/>)
    }
    else{
      toRender = (<span/>)
    }
    return (
      <div>
        {toRender}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  state.registration = null
  return state
}

const mapDispatchToProps = (dispatch) => {
  return {
    setSnapTimer: (snapTimer) => {
      dispatch(setSnapTimer(snapTimer))
    }
  }
}

ChatPagePreloader = connect(mapStateToProps, mapDispatchToProps)(ChatPagePreloader)

export default ChatPagePreloader
