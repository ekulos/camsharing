import React from 'react';
import { connect } from 'react-redux'
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import ActionAlarm from 'material-ui/svg-icons/action/alarm';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentRemove from 'material-ui/svg-icons/content/remove';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import {
  setSnapTimer
} from '../../actions'

class SnapTimerSubmenu extends React.Component {

  componentWillMount(){
    this.incrementSnapTimer = this.incrementSnapTimer.bind(this)
    this.decrementSnapTimer = this.decrementSnapTimer.bind(this)
    this.checkSnapTimer = this.checkSnapTimer.bind(this)
  }

  checkSnapTimer(a){
    let ret = a
    if(!a){
      //Valore di default di a è 5 minuti
      ret = 5
    }
    return ret
  }

  incrementSnapTimer(a, b){
    let ret
    a = this.checkSnapTimer(a)
    ret = a + b
    return ret
  }

  decrementSnapTimer(a, b){
    let ret
    a = this.checkSnapTimer(a)
    let c = a - b
    if (c > 0){
      ret = c
    }
    else{
      ret = a
    }
    return ret
  }

  render(){

    const timingCases = [
      {
        label: "1 minute",
        val: 1
      },
      {
        label: "2 minutes",
        val: 2
      },
      {
        label: "5 minutes",
        val: 5
      }
    ]

    const menuItemCases = (icon, cb) => {
      let list = []
      let item
      let numCases = timingCases.length
      let index
      let newVal

      for (index in timingCases){
        newVal = cb(this.props.snapTimer, timingCases[index].val)
        item = <MenuItem
                  primaryText={timingCases[index].label}
                  leftIcon={icon}
                  onClick={this.props.setSnapTimer.bind(this, newVal, this.props.handleRequestClose)}
                />
        list.push(item)
        if(index < (numCases-1)){
          list.push(<Divider />)
        }
      }
      return list
    }


    return(
      <MenuItem
        primaryText="Snap Timer"
        rightIcon={<ArrowDropRight />}
        menuItems={[
          <MenuItem
            leftIcon={<ActionAlarm />}
            primaryText="Reset default: 5 min"
            onClick={this.props.setSnapTimer.bind(this, 5, this.props.handleRequestClose)}
          />,
          <Divider />,
          <MenuItem
            primaryText="Increment"
            rightIcon={<ArrowDropRight />}
            menuItems={menuItemCases(<ContentAdd />, this.incrementSnapTimer)}
          />,
          <Divider />,
          <MenuItem
            primaryText="Decrement"
            rightIcon={<ArrowDropRight />}
            menuItems={menuItemCases(<ContentRemove />, this.decrementSnapTimer)}
          />
        ]}
      />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ownProps,
      snapTimer: state.chat.snapTimer
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    setSnapTimer: (snapTimer, closeMenu) => {
      closeMenu()
      dispatch(setSnapTimer(snapTimer))
    },
  }
}

SnapTimerSubmenu = connect(mapStateToProps, mapDispatchToProps)(SnapTimerSubmenu)

export default SnapTimerSubmenu
