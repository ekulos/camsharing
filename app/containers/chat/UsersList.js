import React from 'react'
import { connect } from 'react-redux'
import {
  initUsersList,
  updateUsersList
} from '../../actions'

class UsersList extends React.Component {
  componentWillMount(){
    this.userConnected = this.userConnected.bind(this)
    this.userDisconnected = this.userDisconnected.bind(this)
    this.initUsersList = this.initUsersList.bind(this)

    const chatProxy = this.props.chatProxy
    chatProxy.onUserConnected(this.userConnected)
    chatProxy.onUserDisconnected(this.userDisconnected)
    chatProxy.onUsersList(this.initUsersList)
  }

  initUsersList(list){
    this.props.initUsersList(list)
  }

  userConnected(username){
    this.props.updateUsersList(this.props.usersList, username, true)
  }

  userDisconnected(username){
    this.props.updateUsersList(this.props.usersList, username, false)
  }

  render() {
    let usersList = this.props.usersList
    let usersTag
    if(usersList){
      usersTag = Object.keys(usersList).map((user) => {
        let calculatedClassName = "chat-user"
        if(usersList[user]){
          calculatedClassName += " logged"
        }
        else{
          calculatedClassName += " not-logged"
        }
        return (<div key={user} className={calculatedClassName}>{user}</div>)
      })
    }
    return (
      <div className="users-list">
        {usersTag}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let ret = {}
  if (state.chat){
    ret = {
      ...ownProps,
      usersList : state.chat.usersList
    }
  }
  return ret
}

const mapDispatchToProps = (dispatch) => {
  return {
    initUsersList: (list) => {
      dispatch(initUsersList(list))
    },
    updateUsersList: (list, username, logged) => {
      dispatch(updateUsersList(list, username, logged))
    }
  }
}

UsersList = connect(mapStateToProps, mapDispatchToProps)(UsersList)

export default UsersList
