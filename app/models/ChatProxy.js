'use strict'
import EventEmitter from 'events'
import io from 'socket.io-client'
import Constants from '../Constants'
import Peer from 'peerjs'

function getChatProxyInstance(){
/*  var instance = window.chatProxyInstance
  if(!instance){
    console.log("istanzio!!!");
    instance = new ChatProxy()
    window.chatProxyInstance = instance
  }
  return instance*/
  let instance = new ChatProxy()
  window.chatProxyInstance = instance
  return instance
}

function ChatProxy() {
  //EventEmitter.call(this)
  this._peers = {}
  this.id = Math.random()
}

ChatProxy.prototype = Object.create(EventEmitter.prototype)

ChatProxy.prototype.setUsername = function (username) {
  this._username = username
}

ChatProxy.prototype.getUsername = function () {
  return this._username
}

ChatProxy.prototype.onMessage = function (cb) {
  this.addListener('NEW_MESSAGE', cb)
}

ChatProxy.prototype.onUsersList = function (cb) {
  this.addListener('INIT_USERS_LIST', cb)
}

ChatProxy.prototype.onMessagesList = function (cb) {
  this.addListener('INIT_MESSAGES_LIST', cb)
}

ChatProxy.prototype.saveSnapDictionary = function (cb) {
  this.addListener('RESPONSE_SNAP_DICT', cb)
}

ChatProxy.prototype.startCall = function (cb) {
  this.addListener('START_CALL', cb)
}

ChatProxy.prototype.closeCall = function (cb) {
  this.addListener('CLOSE_CALL', cb)
}

ChatProxy.prototype.updateSnapDictionary = function (cb) {
  this.addListener('UPDATE_SNAP_DICT', cb)
}

ChatProxy.prototype.onUserConnected = function (cb) {
  this.addListener('USER_CONNECTED', cb)
}

ChatProxy.prototype.onUserDisconnected = function (cb) {
  this.addListener('USER_DISCONNECTED', cb)
}

ChatProxy.prototype.onChangeAvailability = function (availability) {
  this.socket.emit('CHANGED_AVAILABILITY', this.getUsername(), availability)
}

ChatProxy.prototype.requestSnap =  function () {
  this.socket.emit('REQUEST_SNAP_DICT')
}

ChatProxy.prototype.requestMessagesList =  function () {
  this.socket.emit('REQUEST_MESSAGE_LIST')
}

ChatProxy.prototype.disconnect = function () {
  this.socket.emit('disconnect')
  this.socket.emit('DISCONNECT', this.getUsername())
  this.removeAllListeners()
  delete window.chatProxyInstance
}

ChatProxy.prototype.connectToServer =  function (username) {
  this.socket = io()
  this.peer = new Peer(username, {
    host: location.hostname,
    port: 9000,
    path: '/chat'
  })
  this.setUsername(username)
  this.socket.emit('SEND_USERNAME', username)

  this.socket.on('SEND_USERS_LIST', (list) => {
    this.emit('INIT_USERS_LIST', list)
  })

  this.socket.on('SEND_MESSAGES_LIST', (list) => {
    this.emit('INIT_MESSAGES_LIST', list)
  })

  this.socket.on('USER_CONNECTED', (newUser) => {
    this.emit('USER_CONNECTED', newUser)
    this._connectTo(newUser)
  })

  this.socket.on('RESPONSE_SNAP_DICT', (snapDictionary) => {
    this.emit('RESPONSE_SNAP_DICT', snapDictionary)
  })

  this.socket.on('UPDATE_SNAP_DICT', (username, params) => {
    this.emit('UPDATE_SNAP_DICT', username, params)
  })

  this.socket.on('USER_DISCONNECTED', (id) => {
      this._disconnectFrom(id)
      this.emit('USER_DISCONNECTED', id)
  })

  this.socket.on('NEW_MESSAGE', (message) => {
    this.emit('NEW_MESSAGE', message)
  })

  this.peer.on('connection', (conn) => {
    /*
      L'evento connection insorge nel momento in cui un peer invoca connect
      con l'id di 'questo' peer
    */
    this._registerPeer(conn.peer, conn)
  })
}

ChatProxy.prototype.prepareEntryDictionary = function (snap, firstConnection) {
  const entryDictionary = {
    username : this.getUsername(),
    snap
  }
  this.socket.emit("SEND_SNAP", entryDictionary, firstConnection)
}

ChatProxy.prototype.registerCallListeners = function (ownStream, camRef) {
  if(ownStream){
    //this.camRef = camRef

    this.peer.on('call', (call) => {
      call.answer(ownStream)
      call.on('stream',(stream) => {
        this.emit('START_CALL', call.peer, stream)
      })
      call.on('close', () => {
        console.log('close non esiste');
        this.emit('CLOSE_CALL')
      })
    })
  }
}

ChatProxy.prototype._connectTo = function (newUser) {
  let conn = this.peer.connect(newUser)
  conn.on('open', () => {
    this._registerPeer(newUser, conn)
  })
}

ChatProxy.prototype._registerPeer = function (username, conn) {
  console.log('Registering', username)
  this._peers[username] = conn
/*
  conn.on('data', (message) => {
    const info = {
      content: message,
      author: username,
      time: new Date()
    }
    this.socket.emit("NEW_MESSAGE", info)
  })*/
}

ChatProxy.prototype.broadcastMessage = function(message){
  this.socket.emit("NEW_MESSAGE", message)
}

ChatProxy.prototype.requestCall = function(interlocutor, streamObj){
  let call = this.peer.call(interlocutor, streamObj)
  call.on('stream', function(stream){
    this.emit('START_CALL', interlocutor, stream)
  }.bind(this))
  call.on('err', function(){
    console.log('errore')
  })
}

ChatProxy.prototype.takeSnapshot = function(camRef, firstConnection){
  const myWidth = camRef.width
  const myHeight = camRef.height

  const myCanvas = document.createElement('canvas')
  myCanvas.width = myWidth
  myCanvas.height = myHeight

  const context = myCanvas.getContext('2d')
  context.drawImage(camRef, 0, 0, myWidth, myHeight)
  let snap = myCanvas.toDataURL('image/png')

  this.prepareEntryDictionary(snap, firstConnection)
  return true
}

ChatProxy.prototype._disconnectFrom = function (username) {
  delete this._peers[username]
}

export default getChatProxyInstance
