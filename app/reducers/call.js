/*
  1) Object.assign = per conservare tutti gli altri valori della sezione (in questo caso
  la sezione è state.call)

  2) Si ritorna un nuovo oggetto e vengono inizializzati solo i valori specificati,
  di quelli precedenti non se ne tiene traccia (questo secondo metodo è stato utilizzato
  per evitare di lasciare i campi interlocutor e interlocutor_streamObj inizializzati alla
  precedente call)
*/

const call = (state=null, action) => {
  switch (action.type) {
    case "RESET_STORE":
      return null
      
    case "SHOW_MODAL":
      return Object.assign(
                            {},
                            state,
                            {
                              activeCall: action.visibility
                            }
                          )

    case "HIDE_MODAL":
      return {
                activeCall: action.visibility
             }

    case "SAVE_CALL_PARAMS":
      const interlocutor_streamObj = action.streamObj
      return {
                interlocutor: action.interlocutor,
                interlocutor_streamObj,
                activeCall: true
              }
    default:
      return state
  }
}

export default call
