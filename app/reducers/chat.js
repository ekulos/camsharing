const chat = (state=null, action) => {
  let ret
  let sd
  let username

  switch (action.type) {
    case "RESET_STORE":
      return null

    case "SAVE_STREAM":
      return {
                ...state,
                streamSrc: action.streamSrc,
                streamObj: action.streamObj
             }

    case "SAVE_SNAP_DICTIONARY":
      let otherUsers = {}
      let currentUser = {}
      sd = action.snapDictionary

      if(sd){
        //L'iterazione posiziona al primo posto la snap dell'utente corrente
        Object.keys(sd).map((username) => {
          if(username === action.username){
            currentUser[username] = sd[username]
          }
          else{
            otherUsers[username] = sd[username]
          }
        })
        ret = {
          ...currentUser,
          ...otherUsers
        }
      }
      if(localStorage.getItem('username')){
        /*
          questo controllo evita che nello store di redux venga salvata la
          variabile anche se l'utente si è disconnesso, provocando uno stato inconsistente.
        */
        if(ret){
          return {
            ...state,
            snapDictionary: {...ret}
          }
        }
      }
      else{
        return null
      }

    case 'UPDATE_SNAP_DICT':
      sd = action.snapDictionary
      username = action.username
      let params = action.params

      if(params.logged === false){
        //caso utente disconnesso
        delete sd[username]
      }
      else{
        if(!sd[username]){
          //caso di nuovo utente appena connesso
          sd[username] = {}
        }
        Object.keys(params).forEach((paramName) => {
          sd[username][paramName] = params[paramName]
        })
      }
      return {
        ...state,
        snapDictionary: {...sd}
      }

    case 'INIT_USERS_LIST':
      return {
        ...state,
        usersList: {...action.list}
      }

    case 'UPDATE_USERS_LIST':
      let updatedList = action.list
      updatedList[action.username] = action.logged

      ret = {
        ...state,
        usersList : { ...updatedList}
      }
      return ret

    case 'SHOW_OVERLAY':
      return {
        ...state,
        visibleOverlay: action.name
      }

    case 'HIDE_OVERLAY':
      return {
        ...state,
        visibleOverlay: null
      }

    case 'SET_SNAP_TIMER':
      return{
        ...state,
        snapTimer: action.snapTimer
      }

    case 'NEW_MESSAGE':
      return {
        ...state,
        notReadMessages: action.notReadMessages + 1
      }

    case 'MESSAGES_READ':
      return {
        ...state,
        notReadMessages: 0
      }

    default:
      return state
  }
}

export default chat
