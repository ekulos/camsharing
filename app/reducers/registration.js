const registration = (state=null, action) => {
  switch(action.type){
    case "RESET_STORE":
      return null
      
    case 'CREDENTIALS_REQUEST':
      console.log('registration request submitted')
      return state

    case 'CREDENTIALS_RESULT':
      return {
        ...state,
        username: action.payload.username,
        status: action.payload.status
      }

    case 'CREDENTIALS_ERROR':
      console.log('registration request failed')
      return state

    case 'ACCOUNT_CHECK_REQUEST':
      console.log('check username request submitted')
      return state

    case 'ACCOUNT_CHECK_RESULT':
      return {
        ...state,
        accountAvailable: action.payload.accountAvailable
      }

    case 'ACCOUNT_CHECK_ERROR':
      console.log('check username request error')
      return state

    default:
      return state
  }
}

export default registration
