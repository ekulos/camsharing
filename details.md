#Camsharing

The application is entirely developed with ReactJs and Redux frameworks. Intent is to realize a web app that provides the most complete communication system as possible, to easliy allow the connected users reach each other.

 There are currently implementations for:

* registration/login system;
* cyclic send/receive webcam snapshots for/from other connected users;
* public shared text-chat;

##Supported browser
Cross-browser support not provided yet. For developing and tests it's been used Goolge Chrome v**50.x** so far.

#### Dependencies

The application uses API calls to MongoDB server v**3.2.8**. For the DB communications, it's used Mongo's default port: http://localhost:27017/

------------

## Development

The project is based on Node.js v**5.7.1**, npm v**3.6.0**

Installing the package:

    npm install

It's used webpack here. The command to run the webpack dev server is:

    npm run start

or more simply:

    npm start

The development server will be running on http://localhost:3001/
(no needed to run the build command first).


## Production

For production enironment is necessary to generate the javascript build file. In order to do this, execute the following command:

    npm run build

the generated files will be found in the **build** directory


**Webpack plugins:**

* https://webpack.github.io/docs/webpack-dev-server.html
* https://webpack.github.io/docs/hot-module-replacement.html
* https://github.com/ericclemmons/npm-install-webpack-plugin

------------


## Issues
The connection with the server depends on socket.io library. This event is triggered after the instantiation of the Peer class by the client. The Peer object belongs to "*peerjs*" v**0.3.x** library. You could experiment an issue releted to that. In order to solve this is necessary to modify the internal library code, inside the node_modules folder (path: node_modules/peerjs/dist/peer). We accomplished this by enclosing the line 555: "*Negotiator._makeOffer(connection);*" within:


    setTimeout(function(){
    	Negotiator._makeOffer(connection);
    }, 50)
<<<<<<< HEAD:details.md
But this trick shouldn't be necessary any longer, after the latest release.
=======
But this trick shouldn't be necessary any longer, after the latest release.
>>>>>>> 07e94a28e9fe103718c9558c09cc787e67c4d787:README.md
