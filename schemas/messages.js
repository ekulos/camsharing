var mongoose = require('mongoose')

var Schema = mongoose.Schema

var MessagesSchema = new Schema({
  author: String,
  content: String,
  time: String
})

module.exports = mongoose.model('messagesSchema', MessagesSchema)
