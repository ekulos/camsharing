var mongoose = require('mongoose')

var Schema = mongoose.Schema

var SnapshotsSchema = new Schema({
  username: String,
  snapObj: {
    snap: String,
    time: String
  }
})

module.exports = mongoose.model('snapshotsSchema', SnapshotsSchema)
