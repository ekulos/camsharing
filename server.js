'use strict'

var PeerServer = require('peer').PeerServer
var apiInvoker = require('request-promise')
var express = require('express')
var Constants = require('./app/Constants.js')
var app = express()
var port = process.env.PORT || 3001
var path = require('path')
var webpack = require('webpack')
var config = require('./webpack.config')
var compiler = webpack(config)
var moment = require('moment')
var cron = require('node-cron')
var bodyParser = require('body-parser')
var mongoose = require('mongoose')
var usersAPI = require('./api/handlers/users')
var messagesAPI = require('./api/handlers/messages')
var snapshotsAPI = require('./api/handlers/snapshots')
var usersAPICall = require('./api/calls/users')
var messagesAPICall = require('./api/calls/messages')
var snapshotsAPICall = require('./api/calls/snapshots')

const app_prefix = ''
const host = 'localhost'
const address = 'http://' + host + ':' + port

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath,
  watchOptions: {
      aggregateTimeout: 300,
      poll: true
  }
}))
app.use(require('webpack-hot-middleware')(compiler))

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

//------------ REGISTERING MONGO HANDLERS ------------

//------------ UsersSchema Section -------------------
app.route('/api/saveUser').post(usersAPI.saveUser)
app.route('/api/getUsersList').get(usersAPI.getUsersList)
app.route('/api/getUser').post(usersAPI.getUser)

//------------ MessagesSchema Section ----------------
app.route('/api/saveMessage').post(messagesAPI.saveMessage)
app.route('/api/getMessagesList').get(messagesAPI.getMessagesList)

//------------ SnapshotSchema Section -----------------
app.route('/api/saveSnapshot').post(snapshotsAPI.saveSnapshot)
app.route('/api/getSnapshotsList').get(snapshotsAPI.getSnapshotsList)

//---------------- END MONGO SECTION ------------------

app.get(app_prefix + '/*', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.use(app_prefix + '/static', express.static(__dirname))
mongoose.connect('mongodb://' + host + ':27017/camSharing');


var expressServer = app.listen(port, null, null, () => {

  //Si sta creando un server attivo sulla porta specificata
  //Il socket è il canale di comunicazione condiviso da tutti i peers
  var io = require('socket.io').listen(expressServer)

  console.log('Listening on port', port)
  var peerServer = new PeerServer({ port: 9000, path: '/chat' })

  var messagesListPromise = messagesAPICall.getMessagesList(apiInvoker, address)
  var usersListPromise = usersAPICall.getUsersList(apiInvoker, address)
  var snapshotListPromise = snapshotsAPICall.getSnapshotsList(apiInvoker, address)
  Promise.all([messagesListPromise, snapshotListPromise, usersListPromise])
  .then((resps) => {
    var messagesList = resps[0]
    var snapDictionary = resps[1]
    var usersList = resps[2]
    /*usersList non è calcolabile da snapDictionary perché quest'ultimo include solo
    gli utenti che hanno inviato almeno una volta una snapshot (esclude quelli senza webcam)*/

    cron.schedule('* */2 * * *', () => {
      //ogni 2 ore

      Object.keys(snapDictionary).map((username)=>{
        let item = snapDictionary[username]
        if(item.logged){
          item.username = username
          item.time = now()
          snapshotsAPICall.saveSnapshot(apiInvoker, address, item)
        }
      })
    })

    io.on('connection', (socket) => {
      // ------ MEMO: Gestire connessione contemporanea dello stesso utente su più browsers
      socket.on('SEND_USERNAME', (username) => {
        console.log('User connected with #', username)
        usersList[username] = true

        if(!snapDictionary[username]){
          snapDictionary[username] = {}
        }
        if(!snapDictionary[username].logged){
          //sending to all clients except sender
          socket.broadcast.emit('USER_CONNECTED', username)

          snapDictionary[username].availability = 'available'
        }

        snapDictionary[username].logged = true

        //solo al client appena connesso
        socket.emit('SEND_USERS_LIST', usersList)
        socket.emit('RESPONSE_SNAP_DICT', snapDictionaryLoggedSubset(snapDictionary))
      })

      socket.on('REQUEST_MESSAGE_LIST', () => {
        // ------ MEMO: Mettere un Limit a messages List -------
        socket.emit('SEND_MESSAGES_LIST', messagesList)
      })

      socket.on('SEND_SNAP', (entryDictionary, firstConnection) => {
        /*
          Si entra ogni volta che un client invia la propria snap al server.
          Si deve considerare il caso in cui l'utente si connette per la prima volta
          o dopo essersi sloggato. In questo caso tutti gli altri peer devono essere
          aggiornati istantaneamente, senza aspettare la cadenza del proprio temporizzatore.
        */
        entryDictionary.time = now()
        const username = entryDictionary.username
        let params

        snapDictionary[username].snap = entryDictionary.snap
        console.log(firstConnection);
        if(firstConnection){
          //snapshotsAPICall.saveSnapshot(apiInvoker, address, entryDictionary)
          params = {
            logged: snapDictionary[username].logged ? snapDictionary[username].logged : true,
            availability: snapDictionary[username].availability ? snapDictionary[username].availability : 'available',
            snap: entryDictionary.snap
          }
          socket.broadcast.emit('UPDATE_SNAP_DICT', username, params)
        }
      })

      //Invia snapDictionary solo a chi l'ha richiesto (basato sul valore di cron del client)
      socket.on('REQUEST_SNAP_DICT', () => {
        socket.emit('RESPONSE_SNAP_DICT', snapDictionaryLoggedSubset(snapDictionary))
      })

      socket.on('CHANGED_AVAILABILITY', (username, availability) => {
        snapDictionary[username].availability = availability

        let params = {
          username,
          availability
        }
        io.emit('UPDATE_SNAP_DICT', username, params)
      })

      socket.on('NEW_MESSAGE', (message) => {
        message.time = now()
        socket.broadcast.emit('NEW_MESSAGE', message)
        messagesAPICall.saveMessage(apiInvoker, address, message)
        messagesList.push(message)
      })

      socket.on('DISCONNECT', function (username) {
        //Quando viene chiuso il browser il client resta connesso
        console.log('With #', username, 'user disconnected.')
        usersList[username] = false
        snapDictionary[username].logged = false

        socket.broadcast.emit('USER_DISCONNECTED', username)

        let params = {
          username,
          logged: false
        }
        socket.broadcast.emit('UPDATE_SNAP_DICT', username, params)
        //socket.emit('RESPONSE_SNAP_DICT', snapDictionaryLoggedSubset(snapDictionary))
      })
    })
  })
})

function now(){
  return (+moment()).toString()
}

function snapDictionaryLoggedSubset(sd){
  /*
    Si calcola un sottoinsieme di snapDictionary perché i clients devono visualizzare
    solo gli utenti loggati (si evita di inviare dati inutilizzati), ma il server deve
    tenere traccia anche delle snapshots di quelli sloggati per salvarle sul db.
  */
  let sdSubset = {}
  Object.keys(sd).map((username) => {
    if(sd[username].logged){
      sdSubset[username] = sd[username]
    }
  })
  return sdSubset
}


/*
// sending to sender-client only
socket.emit('message', 'this is a test')

// sending to all clients, include sender
io.emit('message', 'this is a test')

// sending to all clients except sender
socket.broadcast.emit('message', 'this is a test')

// sending to all clients in 'game' room(channel) except sender
socket.broadcast.to('game').emit('message', 'nice game')

// sending to all clients in 'game' room(channel), include sender
io.in('game').emit('message', 'cool game')

// sending to sender client, only if they are in 'game' room(channel)
socket.to('game').emit('message', 'enjoy the game')

// sending to all clients in namespace 'myNamespace', include sender
io.of('myNamespace').emit('message', 'gg')

// sending to individual socketid
socket.broadcast.to(socketid).emit('message', 'for your eyes only')
*/
