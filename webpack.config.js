const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')

var ExtractTextPlugin = require('extract-text-webpack-plugin')

const app_prefix = ''

const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'build')
};

const common = {
  entry: {
    app: PATHS.app
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  output: {
    path: PATHS.build,
    filename: 'bundle.js',
    publicPath: app_prefix + '/build/'
  },
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css!sass'),
        include: PATHS.app
      },
      {
         test: /\.css$/,
         loader: ExtractTextPlugin.extract('style-loader', 'css-loader'),
         include: PATHS.app
      },
      {
        test: /\.css$/,
        loader: 'style!css?modules',
        include: /flexboxgrid/
      },
      {
        test: /\.js?$/,
        loader: 'babel',
        include: PATHS.app,
        query: {
          cacheDirectory: true
        }
      },
      {
        test: /\.js?$/,
        loader: 'babel',
        include: path.join(__dirname, 'api'),
        query: {
          cacheDirectory: true
        }
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=application/font-woff'
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=application/octet-stream'
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&mimetype=image/svg+xml'
      },
      {
        test: /\.ico$|\.jpe?g$|\.gif$|\.png$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        // loader: 'url-loader?prefix=app&limit=1000'
        loader: 'url-loader?limit=10000'
      }
    ]
  }
}

if (process.env.NODE_ENV === 'production') {
  module.exports = merge(common, {
    // devtool: 'source-map',
    plugins: [
      new ExtractTextPlugin('main.css',  { allChunks: true } ),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.DefinePlugin({
         'process.env': {
         'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      })
    ]
  })
} else { // development environment
  module.exports = merge(common, {
    entry: [
      'webpack-hot-middleware/client',
      './app/index'
    ],
    devtool: 'eval-source-map',
    devServer: {
      contentBase: PATHS.build,
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,
      stats: 'errors-only',
      host: process.env.HOST,
      port: process.env.PORT
    },
    plugins: [
      new ExtractTextPlugin('main.css',  { allChunks: true } ),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin()
    ]
  })
}
